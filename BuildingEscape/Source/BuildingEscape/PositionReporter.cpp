// Fill out your copyright notice in the Description page of Project Settings.

#include "PositionReporter.h"
#include "GameFramework/Actor.h"

UPositionReporter::UPositionReporter()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UPositionReporter::BeginPlay()
{
	Super::BeginPlay();

	AActor* Object = GetOwner();
	FString ObjectName = Object->GetName();
	FString ObjectPosition = Object->GetActorLocation().ToString();
	UE_LOG(LogTemp, Warning, TEXT("%s is at %s"), *ObjectName, *ObjectPosition);
}

void UPositionReporter::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

